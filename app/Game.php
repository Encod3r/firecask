<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'status',
        'description',
    ];

    /**
     * Relationships
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_plays_games')->withPivot('start_datetime', 'end_datetime', 'result');
    }
}
