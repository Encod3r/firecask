<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

use App\Game;

use App\Http\Requests\Game\StoreGameRequest;

use App\Http\Resources\GameIndexResource;
use App\Http\Resources\GameResource;

use Symfony\Component\HttpFoundation\Response;

class GameController extends Controller
{
    use SoftDeletes;

    private $fillable;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $model = new Game();
        $this->fillable = $model->getFillable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $games = Game::all();

        if ($request->has('q')) {
            $games = $this->search($request);
        }
        
        return GameIndexResource::collection($games);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreGame  $request
     * @return Response
     */
    public function store(StoreGameRequest $request)
    {
        try {
            $validated_attributes = $request->validated();

            $fillable_attributes = collect($validated_attributes)->only($this->fillable);

            $game = Game::create($fillable_attributes->all());

            return new GameResource($game);
        } catch (Exception $e) {
            return response()->json(['error' => $e], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function search(Request $request)
    {
        try {
            $needle = $request->get('q');
            $result = Game::where('name', 'LIKE', "%$needle%")
                ->orWhere('description', 'LIKE', "%$needle%")
                ->get();
            
            if (! empty($result)) {
                return $result;
            } else {
                return response()->json(['error' => $e], Response::NOT_FOUND);
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        return new GameResource($game->load('users'));
    }
}
