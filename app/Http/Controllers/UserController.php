<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Symfony\Component\HttpFoundation\Response;

use App\User;

use App\Http\Requests\User\StoreUserRequest;

use App\Http\Resources\UserIndexResource;
use App\Http\Resources\UserResource;
class UserController extends Controller
{
    private $fillable;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $model = new User();
        $this->fillable = $model->getFillable();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        
        return UserIndexResource::collection($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUser  $request
     * @return Response
     */
    public function store(StoreUserRequest $request)
    {
        try {
            $validated_attributes = $request->validated();

            $fillable_attributes = collect($validated_attributes)->only($this->fillable);
            $fillable_attributes['password'] = Hash::make($fillable_attributes['password']);
            $user = User::create($fillable_attributes->all());

            return new UserResource($user);
        } catch (Exception $e) {
            return response()->json(['error' => $e], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResource($user->load('games'));
    }
}
// php artisan make:controller UserController --resources --model User