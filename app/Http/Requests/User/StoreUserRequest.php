<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\CheckIfPasswordFormatIsCorrect;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:1|max:255',
            'last_name' => 'required|string|min:1|max:255',
            'email' => 'required|email|unique:users,email|unique:users,email',
            'password' => ['required', 'string', 'confirmed', 'min:8', 'max:64', new CheckIfPasswordFormatIsCorrect()],
            'privacy' => 'required|boolean|in:1',
            'date_of_birth' => 'required|date|date_format:"Y-m-d"',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email is required, bitch!',
            'firstName.required' => 'Firstname is required, bitch!',
            'lastName.required' => 'Lastname is required, bitch!',
            'password.required' => 'Password is required, bitch!',
            'dob.required' => 'Date of birth is required, bitch!',
        ];
    }
}
