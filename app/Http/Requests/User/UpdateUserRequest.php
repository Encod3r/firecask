<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'firstName' => 'required|min:1|max:100',
            'lastName' => 'required|min:1|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'dob' => 'nullable|date',
        ];

        return $rules;
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'firstName.required' => 'Your first name is required, bitch!',
            'lastName.required' => 'Your last name is required, bitch!',
            'email.required' => 'E-mail is required, bitch!',
            'password.required' => 'Password is required, bitch!',
            'dob.required' => 'Date of birth is required, bitch!',
        ];
    }
}
