<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Post::class, function (Faker $faker) {
    $min_user = User::min('id');
    $max_user = User::max('id');
    return [        
        'title' => $faker->sentence(),
        'body' => $faker->paragraph(),
        'author_id' => $faker->numberBetween($min_user, $max_user),
        'status' => $faker->randomElement($array = array ('draft','public','trashed','private')),
    ];
});
