<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::group([
        'middleware' => 'auth:api',
    ], function ($router) {
        Route::post('logout', 'AuthController@logout');
        Route::get('me', 'AuthController@me');
    });
    Route::post('login', 'AuthController@login');
});

/**
 * CRUD routes
 */

// Users
Route::group([
    'middleware' => 'api',
    'prefix' => 'users'
], function ($router) {
    Route::get('/{user}', 'UserController@show');
    Route::get('/', 'UserController@index');

    Route::group([
        'middleware' => 'auth:api',
    ], function ($router) {
        Route::post('/', 'UserController@store');
    });
});
// Games
Route::group([
    'middleware' => 'api',
    'prefix' => 'games'
], function ($router) {
    Route::get('/{game}', 'GameController@show');
    Route::get('/', 'GameController@index');
    
    Route::group([
        'middleware' => 'auth:api',
    ], function ($router) {
        Route::post('/', 'GameController@store');
    });
});
