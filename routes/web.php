<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/about', function () {
//     return view('about');
// });

// // ----------------------------USERS------------------------------- //

// Route::get('/users', 'UserController@index');

// Route::get('/users/create', 'UserController@create');
// // This POST method is called by the form in create
// Route::post('/users', 'UserController@store');

// Route::get('/users/{user}/edit', 'UserController@edit');
// // PUT updates the whole resource (and not a part of it) (by replacing it with a new version)
// Route::put('/users/{user}', 'UserController@update');

// Route::delete('users/{user}/delete', 'UserController@destroy');

// // ----------------------------POSTS------------------------------- //

// // NOTE: first put STATIC routes and then DYNAMIC routes.
// Route::get('/posts', 'PostController@index');
// Route::get('/posts/create', 'PostController@create');
// Route::post('/posts', 'PostController@store');

// Route::get('/posts/{post}/edit', 'PostController@edit');
// // PUT updates the whole resource (and not a part of it) (by replacing it with a new version)
// Route::put('/posts/{post}', 'PostController@update');

// // Passing post as var to use Laravel's power: in the controller I don't have to perform a find.
// Route::get('/posts/{post}', 'PostController@show');

// Route::delete('/posts/{post}', 'PostController@destroy');

// // ----------------------------COMMENTS------------------------------- //

// Route::post('/posts/{post}/new/comment', 'CommentController@store');

